import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { IconButton } from '@material-ui/core';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';
import CssBaseline from '@material-ui/core/CssBaseline';

import { Icon } from '@iconify/react';
import gitlabIcon from '@iconify-icons/simple-icons/gitlab';

import About from './views/about';
import Home from './views/home/home';
import './main.css';

// import Projects from './views/projects';
import ContactMe from './views/contactme';
const darkTheme = createMuiTheme({
    palette: {
        background: {
            default: 'rgb(42, 41, 40)',
        },
        primary: {
            main: 'rgb(246,244,242)',
        },
        secondary: {
            main: 'rgb(255,75,75)',
        },
        text: {
            primary: 'rgb(246,244,242)',
        },
    },
});

function HideOnScroll(props) {
    const { children, window } = props;
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({ target: window ? window() : undefined });

    return (
        <Slide appear={false} direction="down" in={!trigger}>
            {children}
        </Slide>
    );
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 'auto',
        background: darkTheme.palette.background.default,
    },
    buttons: {
        marginLeft: 'auto',
    },
    buttonsHidden: {
        marginLeft: 'auto',
        visibility: 'hidded',
    },
}));

export default function App() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const onLearnMoreClick = () => {
        setValue(1);
    };

    return (
        <ThemeProvider theme={darkTheme}>
            <CssBaseline />
            <div className={classes.root}>
                <HideOnScroll>
                    <AppBar
                        position="sticky"
                        className={classes.appBar}
                        elevation={0}
                    >
                        <div className={classes.buttons}></div>
                        <Tabs
                            value={value}
                            indicatorColor="secondary"
                            onChange={handleChange}
                        >
                            <Tab
                                label={
                                    <span style={{ color: 'white' }}>Home</span>
                                }
                                {...a11yProps(0)}
                            />
                            <Tab
                                label={
                                    <span style={{ color: 'white' }}>
                                        about
                                    </span>
                                }
                                {...a11yProps(1)}
                            />
                            {/* <Tab label="projects" {...a11yProps(2)} /> */}
                            <Tab
                                label={
                                    <span style={{ color: 'white' }}>
                                        Contact me
                                    </span>
                                }
                                {...a11yProps(2)}
                            />
                        </Tabs>
                        <div className={classes.buttons}>
                            <IconButton
                                className="icon"
                                href={'https://gitlab.com/lcrol'}
                            >
                                <Icon className="icon" icon={gitlabIcon} />
                            </IconButton>
                            <IconButton
                                className="icon"
                                href={
                                    'https://www.linkedin.com/in/logan-croley-bb6000172/'
                                }
                            >
                                <LinkedInIcon className="icon" />
                            </IconButton>
                        </div>
                    </AppBar>
                </HideOnScroll>
                <TabPanel value={value} index={0}>
                    <Home onLearnMoreClickCallback={onLearnMoreClick} />
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <About />
                </TabPanel>
                {/* <TabPanel value={value} index={2}>
                <Projects />
            </TabPanel> */}
                <TabPanel value={value} index={2}>
                    <ContactMe />
                </TabPanel>
            </div>
        </ThemeProvider>
    );
}
