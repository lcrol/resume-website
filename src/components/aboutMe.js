import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Avatar, Typography } from '@material-ui/core';
import logan from '../static/logan.jpg';

const useStyles = makeStyles((theme) => ({
    summaryContainer: {
        margin: '7rem 0rem 0rem 0rem',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageContainer: {
        margin: '5rem 0rem 0rem 0rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: '30rem',
        height: '30rem',
    },
}));

const AboutMe = () => {
    const classes = useStyles();
    return (
        <div>
            <Grid container>
                <Grid item lg={6} md={12} className={classes.imageContainer}>
                    <Avatar
                        src={logan}
                        variant="rounded"
                        className={classes.image}
                        style={{ borderRadius: '25px' }}
                    />
                </Grid>

                <Grid item lg={6} md={12} className={classes.summaryContainer}>
                    <Typography variant="h4">
                        Hi, I am Logan, a student at Christopher Newport
                        University and occasional freelancer.
                    </Typography>
                    <br></br>
                    <Typography variant="body1" paragraph>
                        In the past few years I have worked as a web developer
                        for Alion and Canon. There, I primarly worked with
                        ReactJS and NodeJS. At Canon, I also worked with opencv,
                        classical computer vision and machine learning with
                        computer vision.
                    </Typography>
                    <Typography variant="body1" paragraph>
                        I have a passion for computer graphics, and in my free
                        time I program games and animations with unity and
                        threejs. I also enjoy working with blender and
                        integrating my creations into Unity.
                    </Typography>
                    <Typography variant="body1" paragraph>
                        When I'm not infront of a screen you can find me
                        outdoors rock climbing and hiking. You can also find me
                        spending time with my two dogs.
                    </Typography>
                </Grid>
            </Grid>
        </div>
    );
};
export default AboutMe;
