import React from 'react';
import { IconButton } from '@material-ui/core';
import LinkedInIcon from '@material-ui/icons/LinkedIn';

import { Icon } from '@iconify/react';
import gitlabIcon from '@iconify-icons/simple-icons/gitlab';

import { makeStyles } from '@material-ui/core/';
import '../main.css';

const useStyles = makeStyles((theme) => ({
    containter: {
        display: 'flex',
        justifyContent: 'space-around',
        flexDirection: 'row',
    },
}));

function Icons() {
    const classes = useStyles();
    return (
        <div className={classes.containter}>
            <IconButton href={'https://gitlab.com/lcrol'}>
                <Icon
                    className="icon"
                    icon={gitlabIcon}
                    style={{ height: '2rem', width: '2rem' }}
                />
            </IconButton>
            <IconButton
                href={'https://www.linkedin.com/in/logan-croley-bb6000172/'}
            >
                <LinkedInIcon
                    className="icon"
                    style={{ height: '2rem', width: '2rem' }}
                />
            </IconButton>
        </div>
    );
}

export default Icons;
