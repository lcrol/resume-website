import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import GanttBlock from './ganttBlock';

// TODO: FIX COLORS

const green = '#60935D';
const orange = '#FA824C';
// const yellow = '#F1E909';
// const red = '#C17767';
const blue = '#3C91E6';

const ExperienceGantt = () => {
    return (
        <React.Fragment>
            <Typography
                variant="h3"
                align="center"
                style={{ marginBottom: '2rem' }}
            >
                My experience
            </Typography>
            <Grid container spacing={3}>
                <Grid container>
                    <Grid item lg={12} md={12}>
                        <GanttBlock
                            color={blue}
                            title="CNU"
                            date="2018-2022"
                            description="Student"
                        ></GanttBlock>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item lg={4} md={0} />
                    <Grid item lg={4} md={12}>
                        <GanttBlock
                            color={orange}
                            title="Alion"
                            date="2020-2021"
                            description="Web Development Intern"
                        ></GanttBlock>
                    </Grid>
                    <Grid item lg={4} md={0} />
                </Grid>
                <Grid container>
                    <Grid item lg={8} md={0} />
                    <Grid item lg={4} md={12}>
                        <GanttBlock
                            color={green}
                            title="Canon Virginia"
                            date="2021~"
                            description="Software Engineer Intern"
                        ></GanttBlock>
                    </Grid>
                </Grid>
            </Grid>
        </React.Fragment>
    );
};

export default ExperienceGantt;
