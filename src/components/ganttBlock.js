import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography, Paper } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: '2rem',
        textAlign: 'center',
        color: theme.palette.text.secondary,
        whiteSpace: 'nowrap',
        marginBottom: '2rem',
        borderRadius: 15,
    },
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
}));

const GanttBlock = (props) => {
    const classes = useStyles();
    const { color, title, description, date } = props;

    return (
        <Paper className={classes.paper} style={{ backgroundColor: color }}>
            <Grid container className={classes.container} spacing={1}>
                <Grid item style={{ marginLeft: '.5rem' }}>
                    <Grid item>
                        <Typography variant="h6">{title}</Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="subtitle1">
                            {description}
                        </Typography>
                    </Grid>
                </Grid>
                <Grid item>
                    <Typography variant="h6">{date}</Typography>
                </Grid>
            </Grid>
        </Paper>
    );
};

export default GanttBlock;
