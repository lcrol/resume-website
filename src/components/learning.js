import { Typography } from '@material-ui/core';
import React from 'react';
import MediaCard from './mediaCard';
import { Grid } from '@material-ui/core';
import reactNative from '../static/react-native.jpg';
import threejs from '../static/threejs.jpg';

const data = [
    {
        img: reactNative,
        infoLink: 'https://reactnative.dev/',
        title: 'React Native',
        description:
            'Recently, I have wanted to expand the platforms that I am able to develop for. With me already being comfortable with React, React Native seemed like a logical next step.',
    },
    {
        img: threejs,
        infoLink: 'https://threejs.org/',
        title: 'ThreeJS',
        description:
            'I have always loved working with 3D animations. ThreeJS allows me to continue my love for 3D in the browser by helping me make dynamic animations.',
    },
];

const Learning = () => {
    return (
        <div>
            <Typography align="center" variant="h3">
                What I'm Learning
            </Typography>
            <Grid
                container
                spacing={2}
                style={{ margin: '3rem 0rem 1rem 0rem' }}
            >
                {data.map((el) => {
                    return (
                        <Grid item xs={12} md={6} align="center">
                            <MediaCard data={el} />
                        </Grid>
                    );
                })}
            </Grid>
        </div>
    );
};

export default Learning;
