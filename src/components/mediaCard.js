import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 240,
    },
});

const MediaCard = (props) => {
    const { infoLink, title, description, img } = props.data;
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardMedia className={classes.media} image={img} title={title} />
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    {title}
                </Typography>
                <Typography
                    variant="body2"
                    color="textSecondary"
                    component="h5"
                >
                    {description}
                </Typography>
            </CardContent>
            <CardActions>
                <Button
                    size="small"
                    color="#3C91E6"
                    href={infoLink}
                    style={{ margin: 'auto' }}
                >
                    Learn More
                </Button>
            </CardActions>
        </Card>
    );
};

export default MediaCard;
