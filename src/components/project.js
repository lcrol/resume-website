import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Avatar, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    summaryContainer: {
        margin: '7rem 0rem 0rem 0rem',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageContainer: {
        margin: '5rem 0rem 0rem 0rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: '20rem',
        height: '20rem',
    },
}));

const Project = (props) => {
    const { img, title, desc } = props.data;
    const classes = useStyles();
    return (
        <div>
            <Grid container>
                <Grid item lg={6} md={12} className={classes.imageContainer}>
                    <Avatar
                        src={img}
                        variant="rounded"
                        className={classes.image}
                        style={{ borderRadius: '25px' }}
                    />
                </Grid>

                <Grid item lg={6} md={12} className={classes.summaryContainer}>
                    <Typography variant="h4">{title}</Typography>
                    <br></br>
                    <Typography variant="body1" paragraph>
                        {desc}
                    </Typography>
                </Grid>
            </Grid>
        </div>
    );
};
export default Project;
