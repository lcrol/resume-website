import React from 'react';
import { makeStyles } from '@material-ui/core/';

const useStyles = makeStyles({
    spacer: {
        margin: '3rem 0rem 3rem 0rem',
    },
});

const Spacer = () => {
    const classes = useStyles();
    return <div className={classes.spacer}></div>;
};
export default Spacer;
