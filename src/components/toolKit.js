import React from 'react';
import { IoLogoJavascript } from 'react-icons/io';
import { FaNodeJs, FaReact, FaPython, FaUnity } from 'react-icons/fa';
import { DiMongodb, DiDjango, DiCss3 } from 'react-icons/di';
import { SiMysql } from 'react-icons/si';
import { AiFillHtml5 } from 'react-icons/ai';
import { BiGitBranch } from 'react-icons/bi';
import { IconContext } from 'react-icons/';
import { Icon } from '@iconify/react';
import opencvIcon from '@iconify-icons/file-icons/opencv';

import ToolKitIcon from './toolKitIcon';
import { Grid } from '@material-ui/core';
import { Typography } from '@material-ui/core';

const data = [
    {
        icon: (
            <IconContext.Provider value={{ size: '2rem' }}>
                <IoLogoJavascript />
            </IconContext.Provider>
        ),
        title: 'Javascript',
    },
    {
        icon: (
            <IconContext.Provider value={{ size: '2rem' }}>
                <FaNodeJs />
            </IconContext.Provider>
        ),
        title: 'nodejs',
    },
    {
        icon: (
            <IconContext.Provider value={{ size: '2rem' }}>
                <FaReact />
            </IconContext.Provider>
        ),
        title: 'React',
    },
    {
        icon: (
            <IconContext.Provider value={{ size: '2rem' }}>
                <AiFillHtml5 />
            </IconContext.Provider>
        ),
        title: 'HTML5',
    },
    {
        icon: (
            <IconContext.Provider value={{ size: '2rem' }}>
                <DiCss3 />
            </IconContext.Provider>
        ),
        title: 'css',
    },

    {
        icon: (
            <IconContext.Provider value={{ size: '2rem' }}>
                <DiMongodb />
            </IconContext.Provider>
        ),
        title: 'Mongodb',
    },
    {
        icon: (
            <IconContext.Provider value={{ size: '3rem' }}>
                <SiMysql />
            </IconContext.Provider>
        ),
    },
    {
        icon: (
            <IconContext.Provider value={{ size: '2rem' }}>
                <FaPython />
            </IconContext.Provider>
        ),
        title: 'python',
    },
    {
        icon: (
            <IconContext.Provider value={{ size: '3rem' }}>
                <DiDjango />
            </IconContext.Provider>
        ),
    },
    {
        icon: (
            <Icon icon={opencvIcon} style={{ width: '2rem', height: '2rem' }} />
        ),
        title: 'opencv',
    },

    {
        icon: (
            <IconContext.Provider value={{ size: '2rem' }}>
                <FaUnity />
            </IconContext.Provider>
        ),
        title: 'Unity',
    },

    {
        icon: (
            <IconContext.Provider value={{ size: '2rem' }}>
                <BiGitBranch />
            </IconContext.Provider>
        ),
        title: 'Git',
    },
];

const ToolKit = () => {
    return (
        <div>
            <Grid container>
                <Grid item sm={12}>
                    <Typography align="center" variant="h3">
                        My Tool Kit
                    </Typography>
                </Grid>
                {data.map((el) => {
                    return (
                        <Grid
                            item
                            sm={3}
                            spacing={2}
                            style={{ margin: '1rem 0rem 1rem 0rem' }}
                        >
                            <ToolKitIcon data={el} />
                        </Grid>
                    );
                })}
            </Grid>
        </div>
    );
};

export default ToolKit;
