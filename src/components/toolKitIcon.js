import React from 'react';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
}));

const ToolKitIcon = (props) => {
    const { icon, title } = props.data;
    const classes = useStyles();
    return (
        <>
            <div className={classes.container}>
                {icon}
                {title ? <Typography>{title}</Typography> : ''}
            </div>
        </>
    );
};

export default ToolKitIcon;
