import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: 'rgb(42, 41, 40)',
        },
        text: {
            main: 'rgb(246,244,242)',
        },
        secondary: {
            main: 'rgb(246,244,242)',
        },
    },
});

export default theme;
