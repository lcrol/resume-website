import React from 'react';
import { Container } from '@material-ui/core';
import AboutMe from '../components/aboutMe';
import ExperienceGantt from '../components/experienceGantt';
import ToolKit from '../components/toolKit';
import Spacer from '../components/spacer';
import Learning from '../components/learning';

const About = () => {
    return (
        <>
            <Container>
                <AboutMe />
                <Spacer />
                <ToolKit />
                <Spacer />
                <Learning />
                <Spacer />
                <ExperienceGantt />
            </Container>
        </>
    );
};

export default About;
