import React from 'react';
import { makeStyles, Typography } from '@material-ui/core/';
import Icons from '../components/contactMe.subnav';

const useStyles = makeStyles((theme) => ({
    containter: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)',
    },
    spacer: { margin: '1rem 0rem 1rem 0rem' },
}));

function ContactMe() {
    const classes = useStyles();
    return (
        <div className={classes.containter}>
            <Typography align="center" variant="h4">
                Like what you see and want to learn more?
            </Typography>
            <div className={classes.spacer} />
            <Typography align="center" variant="h5">
                Contact me @ logancroley@gmail.com
            </Typography>
            <div className={classes.spacer} />
            <Icons />
        </div>
    );
}

export default ContactMe;
