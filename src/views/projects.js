import { Container, Grid, Typography } from '@material-ui/core';
import React from 'react';
import Project from '../components/project';
import reactNative from './react-native.jpg';

const data = [
    {
        img: reactNative,
        title: 'Ex irure non nulla anim nulla amet.',

        desc: 'Cillum proident nisi eiusmod adipisicing Lorem cillum officia enim veniam. Cupidatat enim dolore velit labore tempor in est aute nisi esse dolore. Sunt esse tempor sint excepteur voluptate. Consequat do culpa in laboris dolor elit tempor incididunt amet nulla. Excepteur sint exercitation adipisicing ea nisi. Veniam eu do non amet. Ullamco aute officia do mollit ex commodo consectetur sint occaecat ut nostrud ipsum. Velit nisi cillum proident consequat laboris proident anim voluptate. Laborum do sit dolore ex ea irure voluptate elit excepteur non aute ut. Proident enim laborum velit eu ad aliqua. Reprehenderit non dolor minim dolore ad sit. Laboris commodo sint velit labore adipisicing et. Ullamco deserunt exercitation ut esse ex non do. Ullamco adipisicing ex et laboris sit. Esse elit eiusmod duis consequat excepteur laborum et officia do adipisicing et. Anim nostrud est qui aliquip aute.   ',
    },
    {
        img: reactNative,
        title: 'Ex irure non nulla anim nulla amet.',

        desc: 'Cillum proident nisi eiusmod adipisicing Lorem cillum officia enim veniam. Cupidatat enim dolore velit labore tempor in est aute nisi esse dolore. Sunt esse tempor sint excepteur voluptate. Consequat do culpa in laboris dolor elit tempor incididunt amet nulla. Excepteur sint exercitation adipisicing ea nisi. Veniam eu do non amet. Ullamco aute officia do mollit ex commodo consectetur sint occaecat ut nostrud ipsum. Velit nisi cillum proident consequat laboris proident anim voluptate. Laborum do sit dolore ex ea irure voluptate elit excepteur non aute ut. Proident enim laborum velit eu ad aliqua. Reprehenderit non dolor minim dolore ad sit. Laboris commodo sint velit labore adipisicing et. Ullamco deserunt exercitation ut esse ex non do. Ullamco adipisicing ex et laboris sit. Esse elit eiusmod duis consequat excepteur laborum et officia do adipisicing et. Anim nostrud est qui aliquip aute.   ',
    },
    {
        img: reactNative,
        title: 'Ex irure non nulla anim nulla amet.',

        desc: 'Cillum proident nisi eiusmod adipisicing Lorem cillum officia enim veniam. Cupidatat enim dolore velit labore tempor in est aute nisi esse dolore. Sunt esse tempor sint excepteur voluptate. Consequat do culpa in laboris dolor elit tempor incididunt amet nulla. Excepteur sint exercitation adipisicing ea nisi. Veniam eu do non amet. Ullamco aute officia do mollit ex commodo consectetur sint occaecat ut nostrud ipsum. Velit nisi cillum proident consequat laboris proident anim voluptate. Laborum do sit dolore ex ea irure voluptate elit excepteur non aute ut. Proident enim laborum velit eu ad aliqua. Reprehenderit non dolor minim dolore ad sit. Laboris commodo sint velit labore adipisicing et. Ullamco deserunt exercitation ut esse ex non do. Ullamco adipisicing ex et laboris sit. Esse elit eiusmod duis consequat excepteur laborum et officia do adipisicing et. Anim nostrud est qui aliquip aute.   ',
    },
    {
        img: reactNative,
        title: 'Ex irure non nulla anim nulla amet.',

        desc: 'Cillum proident nisi eiusmod adipisicing Lorem cillum officia enim veniam. Cupidatat enim dolore velit labore tempor in est aute nisi esse dolore. Sunt esse tempor sint excepteur voluptate. Consequat do culpa in laboris dolor elit tempor incididunt amet nulla. Excepteur sint exercitation adipisicing ea nisi. Veniam eu do non amet. Ullamco aute officia do mollit ex commodo consectetur sint occaecat ut nostrud ipsum. Velit nisi cillum proident consequat laboris proident anim voluptate. Laborum do sit dolore ex ea irure voluptate elit excepteur non aute ut. Proident enim laborum velit eu ad aliqua. Reprehenderit non dolor minim dolore ad sit. Laboris commodo sint velit labore adipisicing et. Ullamco deserunt exercitation ut esse ex non do. Ullamco adipisicing ex et laboris sit. Esse elit eiusmod duis consequat excepteur laborum et officia do adipisicing et. Anim nostrud est qui aliquip aute.   ',
    },
    {
        img: reactNative,
        title: 'Ex irure non nulla anim nulla amet.',

        desc: 'Cillum proident nisi eiusmod adipisicing Lorem cillum officia enim veniam. Cupidatat enim dolore velit labore tempor in est aute nisi esse dolore. Sunt esse tempor sint excepteur voluptate. Consequat do culpa in laboris dolor elit tempor incididunt amet nulla. Excepteur sint exercitation adipisicing ea nisi. Veniam eu do non amet. Ullamco aute officia do mollit ex commodo consectetur sint occaecat ut nostrud ipsum. Velit nisi cillum proident consequat laboris proident anim voluptate. Laborum do sit dolore ex ea irure voluptate elit excepteur non aute ut. Proident enim laborum velit eu ad aliqua. Reprehenderit non dolor minim dolore ad sit. Laboris commodo sint velit labore adipisicing et. Ullamco deserunt exercitation ut esse ex non do. Ullamco adipisicing ex et laboris sit. Esse elit eiusmod duis consequat excepteur laborum et officia do adipisicing et. Anim nostrud est qui aliquip aute.   ',
    },
    {
        img: reactNative,
        title: 'Ex irure non nulla anim nulla amet.',

        desc: 'Cillum proident nisi eiusmod adipisicing Lorem cillum officia enim veniam. Cupidatat enim dolore velit labore tempor in est aute nisi esse dolore. Sunt esse tempor sint excepteur voluptate. Consequat do culpa in laboris dolor elit tempor incididunt amet nulla. Excepteur sint exercitation adipisicing ea nisi. Veniam eu do non amet. Ullamco aute officia do mollit ex commodo consectetur sint occaecat ut nostrud ipsum. Velit nisi cillum proident consequat laboris proident anim voluptate. Laborum do sit dolore ex ea irure voluptate elit excepteur non aute ut. Proident enim laborum velit eu ad aliqua. Reprehenderit non dolor minim dolore ad sit. Laboris commodo sint velit labore adipisicing et. Ullamco deserunt exercitation ut esse ex non do. Ullamco adipisicing ex et laboris sit. Esse elit eiusmod duis consequat excepteur laborum et officia do adipisicing et. Anim nostrud est qui aliquip aute.   ',
    },
    {
        img: reactNative,
        title: 'Ex irure non nulla anim nulla amet.',

        desc: 'Cillum proident nisi eiusmod adipisicing Lorem cillum officia enim veniam. Cupidatat enim dolore velit labore tempor in est aute nisi esse dolore. Sunt esse tempor sint excepteur voluptate. Consequat do culpa in laboris dolor elit tempor incididunt amet nulla. Excepteur sint exercitation adipisicing ea nisi. Veniam eu do non amet. Ullamco aute officia do mollit ex commodo consectetur sint occaecat ut nostrud ipsum. Velit nisi cillum proident consequat laboris proident anim voluptate. Laborum do sit dolore ex ea irure voluptate elit excepteur non aute ut. Proident enim laborum velit eu ad aliqua. Reprehenderit non dolor minim dolore ad sit. Laboris commodo sint velit labore adipisicing et. Ullamco deserunt exercitation ut esse ex non do. Ullamco adipisicing ex et laboris sit. Esse elit eiusmod duis consequat excepteur laborum et officia do adipisicing et. Anim nostrud est qui aliquip aute.   ',
    },
];
function Projects() {
    return (
        <Container>
            <Grid container>
                <Grid item xs={12}>
                    <Typography
                        variant="h2"
                        align="center"
                        style={{ margin: '3rem 0rem 0rem 0rem' }}
                    >
                        My Work
                    </Typography>
                </Grid>
                {data.map((el) => {
                    return (
                        <Grid item xs={12}>
                            <Project data={el} />
                        </Grid>
                    );
                })}
            </Grid>
        </Container>
    );
}

export default Projects;
